package net.guides.springboot2.crud.service;

import net.guides.springboot2.crud.exception.ResourceNotFoundException;
import net.guides.springboot2.crud.model.Employee;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;


public interface EmployeeService {
     List<Employee> getAllEmployees();
      Employee getEmployeeById(Long employeeId) throws ResourceNotFoundException;
     Employee createEmployee(Employee employee);

    public Employee updateEmployee(Long employeeId,Employee employee) throws ResourceNotFoundException;

    void deleteEmployee( Long employeeId) throws ResourceNotFoundException;


}
