package net.guides.springboot2.springboot2jpacrudexample;

import net.guides.springboot2.crud.Application;
import net.guides.springboot2.crud.exception.ResourceNotFoundException;
import net.guides.springboot2.crud.model.Employee;
import net.guides.springboot2.crud.repository.EmployeeRepository;
import net.guides.springboot2.crud.service.EmployeeService;
import net.guides.springboot2.crud.service.EmplyeeServiceImpl;
import org.junit.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Description;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApplicationTests {

	@InjectMocks
	EmplyeeServiceImpl employeeService;

	@Mock
	EmployeeRepository employeeRepository;


	@BeforeEach
	List<Employee>  setUp(){
		System.out.println("##### Set Up tests #######");
		List<Employee> initialList =new ArrayList<>();
		initialList.add(new Employee("user1","user1","user1.gmail.com"));
		initialList.add(new Employee("user2","user2","user2.gmail.com"));
		initialList.add(new Employee("user3","user3","user3.gmail.com"));
        return initialList;

	}

	@BeforeClass
     public static void beforAnyCall(){
		System.out.println("##### BeforeClass any test #######");
	}

	@Before
	public void  before(){
		System.out.println("##### befor any test #######");
	}

	@After
	public void  after(){
		System.out.println("##### after any test #######");
	}
	@Test
	@Description("test_get_all_employee_should_return_list_of_3_elments")
	public void test_get_all_employee_should_return_list_of_3_elments(){
        when(employeeRepository.findAll()).thenReturn(setUp());
		List<Employee> employeeList = employeeService.getAllEmployees();
		assertNotNull(employeeList);
		assertEquals(employeeList.size(),3);
	}

	@Test
	@Description("test empty data base ")
	public void test_get_all_employee_should_return_emptyArray(){
		when(employeeRepository.findAll()).thenReturn(new ArrayList<>());
		List<Employee> employeeList = employeeService.getAllEmployees();
		assertNotNull(employeeList);
		assertEquals(employeeList.size(),0);
	}

	@Test
	@Description("get employee by id should return no null value ")
	public void test_get_employe_id_should_return_existen_employee() throws ResourceNotFoundException {
		Employee employee=new Employee("user1","user1","user1.gmail.com");
		Long id= 4L;
		when(employeeRepository.findById(id)).thenReturn(Optional.of(employee));
		Employee employee1 = employeeService.getEmployeeById(id);
		assertNotNull(employee1);
		assertEquals(employee1.getFirstName(),"user1");
		assertEquals(employee1.getLastName(),"user1");
		assertEquals(employee1.getEmailId(),"user1.gmail.com");

	}

	@Test
	@Ignore
	@Description("test no found employee ")
	public void test_get_employe_id_should_return_ResourceNotFoundException() throws ResourceNotFoundException {
		Long id = 4L;
		//doThrow((new ResourceNotFoundException("Employee not found for this id :: " + id))).when(employeeRepository).findById(id);
		when(employeeRepository.findById(id)).thenThrow(new ResourceNotFoundException("Employee not found for this id :: " + id));
		//assertThrows(ResourceNotFoundException.class,()-> employeeService.getEmployeeById(id));
		//assertEquals("Employee not found for this id :: " + id,exception.getMessage());
		// Invoke the method under test
		try {
			employeeService.getEmployeeById(id);
			fail("Expected YourCheckedException to be thrown");
		} catch (ResourceNotFoundException e) {
			// Exception is expected, so we can assert its properties if needed
			assertEquals("Employee not found for this id :: " + id, e.getMessage());
		}


	}

	@Test
	@Description("test add new employee")
	public void test_add_employee_should_return_employee_added(){
		Employee employee=new Employee("user1","user1","user1.gmail.com");
		when(employeeRepository.save(employee)).thenReturn(employee);
		Employee result=employeeService.createEmployee(employee);
		assertNotNull(result);
		assertEquals(employee.getLastName(),result.getLastName());
	}

}
