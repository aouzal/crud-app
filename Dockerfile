from dockette/jdk8

WORKDIR /app

COPY ./target/springboot2-postgresql-jpa-hibernate-crud-example-0.0.1-SNAPSHOT.jar /app/springboot2-postgresql-jpa-hibernate-crud-example-0.0.1-SNAPSHOT.jar

EXPOSE 8080

CMD ["java","-jar","springboot2-postgresql-jpa-hibernate-crud-example-0.0.1-SNAPSHOT.jar"]

